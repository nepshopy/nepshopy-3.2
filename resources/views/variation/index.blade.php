@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('variation.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Variation')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Variation')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Attributes')}}</th>
                    <th>{{__('Variations')}}</th>
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($variations as $key => $variation)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>
                        @foreach($attributes as $key=>$attribute)
                        @if(json_decode($variation->attribute_id) == $attribute->id) 
                        {{ $attribute->name }}
                        @endif
                        @endforeach
                    </td>
                    <td>
                        <?php 
                        $arr = explode(',',$variation->variation_name); 
                        foreach($arr as $i) 
                            echo('-'.'&nbsp;'.$i.'<br>'); 
                        ?> 
                    </td>
                    <td>
                        <div class="btn-group dropdown">
                            <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                {{__('Actions')}} <i class="dropdown-caret"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{route('variation.edit', encrypt($variation->id))}}">{{__('Edit')}}</a></li>
                                <li><a onclick="confirm_modal('{{route('variation.destroy', $variation->id)}}');">{{__('Delete')}}</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection
